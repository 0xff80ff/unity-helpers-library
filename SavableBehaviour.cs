﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;

namespace UHL
{
  public class SavableBehaviour<Data> : MonoBehaviour
    where Data : SavableBehaviour<Data>.MetaData, new()
  {
    [System.Serializable]
    public class MetaData
    {
      public System.DateTime saveTimeUTC;
      public byte hash;
    }

    public bool binary = true;
    public Data data = new Data();

    string GetFilePath()
    {
      return string.Format("{0}/{1}_{2}.bin", Application.persistentDataPath, gameObject.name, typeof(Data));
    }

    byte GetDataHash()
    {
      byte oldHash = data.hash;
      data.hash = 0;
      BinaryFormatter bf = new BinaryFormatter();
      byte[] dataBytes;
      using (var ms = new MemoryStream())
      {
        bf.Serialize(ms, data);
        dataBytes = ms.ToArray();
      }
      byte result = 0xD3;
      foreach (byte b in dataBytes)
        result ^= b;
      data.hash = oldHash;
      return result;
    }

    public void Save()
    {
      data.saveTimeUTC = System.DateTime.UtcNow;
      data.hash = GetDataHash();

      using (FileStream file = File.Create(GetFilePath()))
      {
        if (binary)
          (new BinaryFormatter()).Serialize(file, data);
        else
        {
          var buffer = JsonUtility.ToJson(data, true);
          file.Write(Encoding.UTF8.GetBytes(buffer), 0, Encoding.UTF8.GetByteCount(buffer));
        }
      }
    }

    public void Load()
    {
      string filepath = GetFilePath();
      try
      {
        bool fileFound = File.Exists(filepath);
        if (fileFound)
        {
          if (binary)
          {
            using (FileStream file = File.Open(filepath, FileMode.Open))
              data = (Data)(new BinaryFormatter()).Deserialize(file);
          }
          else
          {
            string json = File.ReadAllText(filepath, Encoding.UTF8);
            JsonUtility.FromJsonOverwrite(json, data);
          }
        }
        OnDataLoaded(fileFound);
      }
      catch (System.Exception e)
      {
        Debug.LogErrorFormat("File {0} : {1}", filepath, e.Message);
        
        OnDataLoaded(false);
      }
    }

    public void Wipe()
    {
      File.Delete(GetFilePath());
    }

    protected virtual void OnDataLoaded(bool success) {}

    public virtual void OnEnable()
    {
      Load();
    }

    public virtual void OnDisable()
    {
      Save();
    }

    public virtual void OnApplicationPause(bool pause)
    {
      if (pause)
        Save();
      else
        Load();
    }

    public virtual void OnApplicationFocus(bool focus)
    {
      if (focus)
        Load();
      else
        Save();
    }
  }
}