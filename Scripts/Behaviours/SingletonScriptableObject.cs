﻿using UnityEngine;

namespace UHL
{
	[DefaultExecutionOrder(int.MinValue / 2)]
	public class SingletonScriptableObject<T> : ScriptableObject
		where T : SingletonScriptableObject<T>
	{
		public static string assetPath { get { return typeof(T).ToString().Replace('.', '/'); } }
		
		private static bool applicationQuit = false;
		private static T _instance;
		public static T instance
		{
			get
			{
				if (_instance == null && !applicationQuit)
					_instance = Resources.Load<T>(assetPath);

				return _instance;
			}
		}

		protected virtual void OnApplicationQuit()
		{
			applicationQuit = true;
		}

#if UNITY_EDITOR
		protected const string DefaultAssetMenuPath = "Assets/Create/Singleton/";

		protected static string assetPathAbsolute { get { return string.Format("Assets/Resources/{0}.asset", assetPath); } }

		/// <returns>True if new asset was created.</returns>
		protected static bool CreateAsset(out T asset)
		{
			asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPathAbsolute);
            
			if (asset && !UnityEditor.EditorUtility.DisplayDialog( "An asset already exists.", "Overwrite it?", "Yes", "No"))
				return false;
			
			asset = ScriptableObject.CreateInstance<T>();

			UnityEditor.AssetDatabase.CreateAsset(asset, string.Format("Assets/Resources/{0}.asset", assetPath));
			UnityEditor.AssetDatabase.SaveAssets();

			UnityEditor.EditorUtility.FocusProjectWindow();

			UnityEditor.Selection.activeObject = asset;

			return true;
		}
#endif
	}
}