﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections.Generic;
using System;

namespace UHL
{
  public class SocialBehaviour : UniqueBehaviour<SocialBehaviour>
  {
    #region Initialization

    protected override void Awake()
    {
      try
      {
        base.Awake();

        #if (UNITY_ANDROID || UNITY_IOS) && !NO_GPGS && !UNITY_EDITOR
        Log("Platform - Google Play Games");
        GooglePlayGames.PlayGamesPlatform.Activate();
        #elif UNITY_IOS
        Log("Platform - Game Center");
        #else
        Log("Platform - Disabled");
        #endif

        Authenticate();

        SortArrays();
      }
      catch (DuplicateException e)
      {
        e.Resolve(false);
      }
    }

    protected void Authenticate()
    {
      if (Social.localUser.authenticated)
        return;

      Log("Login Start");
      Social.localUser.Authenticate((bool success) =>
      {
        Log(success ? "Login Finished" : "Login Failed");
      });
    }

    #endregion

    #region Data

    [Serializable]
    public class Nameable<Name> : IComparable
      where Name : IComparable
    {
      public Name name;

      public Nameable(Name n)
      {
        name = n;
      }

      public int CompareTo(object otherObject)
      {
        var other = ((Nameable<Name>)otherObject);
        int result = 1;
        if (name == null)
        {
          if (other.name == null)
            result = 0;
        }
        else
          result = name.CompareTo(other.name);

        return result;
      }
    }

    [Serializable]
    public class PlatformedID : Nameable<RuntimePlatform>
    {
      public PlatformedID(RuntimePlatform n) : base(n) {}

      public string id;
    };

    [Serializable]
    public abstract class Publishable<PublishData> : Nameable<string>
    {
      public Publishable(string n) : base(n) {}

      public List<PlatformedID> ids;
      protected bool published = false;

      public void Update(PublishData data)
      {
        published = false;
        OnUpdate(data);
      }

      protected abstract void OnUpdate(PublishData data);

      public void Publish()
      {
        if (published || !Social.localUser.authenticated)
          return;

        int idIndex = ids.BinarySearch(new PlatformedID(Application.platform));
        if (idIndex < 0)
          return;
        
        OnPublish(ids[idIndex].id);
      }

      protected abstract void OnPublish(string id);
    }

    [Serializable]
    public class Board : Publishable<long>
    {
      public Board(string n) : base(n) {}

      public enum Select
      {
        Max,
        Min
      }

      public long score = 0;
      public Select select = Select.Max;

      protected override void OnUpdate(long candidate)
      {
        if ((select == Select.Max && candidate > score) || (select == Select.Min && candidate < score))
          score = candidate;
      }

      protected override void OnPublish(string id)
      {
        Log("Score {0}:={1} Started", name, score);
        Social.ReportScore(score, id, (success) =>
        {
          Log("Score {0}:={1} {2}", name, score, success ? "Finished" : "Failed");
          published = success;
        });
      }
    }

    [Serializable]
    public class Achievement : Publishable<double>
    {
      public Achievement(string n) : base(n) {}

      public double progress = -1;

      protected override void OnUpdate(double newProgress)
      {
        progress = newProgress;
      }

      protected override void OnPublish(string id)
      {
        if (progress < 0)
          return;
        
        Log("Achievement {0}:={1} Started", name, progress);
        Social.ReportProgress(id, progress, (success) =>
        {
          Log("Achievement {0}:={1} {2}", name, progress, success ? "Finished" : "Failed");
          published = success;
        });
      }
    }

    public List<Board> boards;

    public List<Achievement> achievements;

    void SortArrays()
    {
      boards.Sort();
      foreach (var board in boards)
        board.ids.Sort();

      achievements.Sort();
      foreach (var achievement in achievements)
        achievement.ids.Sort();
    }

    #endregion

    #region Implementation

    public void UpdateScore(string scoreName, long value)
    {
      int boardIndex = boards.BinarySearch(new Board(scoreName));
      if (boardIndex < 0)
        return;

      boards[boardIndex].Update(value);

      foreach (var b in boards)
        b.Publish();
    }

    public void Achieve(string achievementName, double progress)
    {
      int achievementIndex = achievements.BinarySearch(new Achievement(achievementName));
      if (achievementIndex < 0)
        return;

      achievements[achievementIndex].Update(progress);

      foreach (var a in achievements)
        a.Publish();
    }

    public void ShowLeaderBoardUI()
    {
      Authenticate();
      Social.ShowLeaderboardUI();
    }

    public void ShowAchievenetsUI()
    {
      Authenticate();
      Social.ShowAchievementsUI();
    }

    #endregion

    #region Utils

    protected static void Log(string format, params object[] objects)
    {
      Debug.LogFormat("Social: " + format, objects);
    }

    #endregion
  }
}