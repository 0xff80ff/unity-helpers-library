﻿using UnityEngine.Events;
using UnityEngine;

namespace UHL
{
	public class UniqueBehaviour<Singleton> : MonoBehaviour
		where Singleton : UniqueBehaviour<Singleton>
	{
		private static Singleton _instance = null;

		public static Singleton instance
		{
			get { return _instance; }
			private set { onInstanceChanged.Invoke(_instance = value); }
		}

		class UnityEventSingleton : UnityEvent<Singleton> { }
		public static readonly UnityEvent<Singleton> onInstanceChanged = new UnityEventSingleton();

		protected virtual void Awake()
		{
			if (instance != null)
				throw new DuplicateException(this as Singleton);

			instance = this as Singleton;
		}

		protected virtual void OnDestroy()
		{
			if (instance as UniqueBehaviour<Singleton> == this)
				instance = null;
		}

		public class DuplicateException : UnityException
		{
			Singleton newInstance;

			public DuplicateException(Singleton conflictNewInstance)
				: base("Duplicate instance of " + typeof(Singleton).ToString())
			{
				newInstance = conflictNewInstance;
			}

			public void Resolve(bool useNew)
			{
				if (useNew)
				{
					GameObject.DestroyImmediate(UniqueBehaviour<Singleton>.instance.gameObject);
					UniqueBehaviour<Singleton>.instance = newInstance;
				}
				else
					GameObject.DestroyImmediate(newInstance.gameObject);
			}
		}
	}
}