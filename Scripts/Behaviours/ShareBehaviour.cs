﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace UHL
{
	public class ShareBehaviour : UniqueBehaviour<ShareBehaviour>
	{
		const string linkFormatLocalAndroid = "market://details?id={0}";
		const string linkFormatLocalIOS = "https://itunes.apple.com/ru/app/{0}";
		const string linkFormatPublicAndroid = "http://play.google.com/store/apps/details?id={0}";
		const string linkFormatPublicIOS = linkFormatLocalIOS;

		public Texture2D image;
		public string text = "Try this awesome game!";
		[Header(linkFormatLocalAndroid)]
		public string appIdAndroid = "com.CompanyName.GameName";
		[Header(linkFormatLocalIOS)]
		public string appIdIOS = "id123456789";

		public string LinkLocal
		{
			get
			{
				return
#if UNITY_IOS
          string.Format(linkFormatLocalIOS, appIdIOS);
#else
		  string.Format(linkFormatLocalAndroid, appIdAndroid);
#endif
			}
		}

		public string LinkPublic
		{
			get
			{
				return
#if UNITY_IOS
          string.Format(linkFormatPublicIOS, appIdIOS);
#else
		  string.Format(linkFormatPublicAndroid, appIdAndroid);
#endif
			}
		}

		public string ShareText
		{
			get
			{
				if (text.Length > 0)
					return string.Format("{0}\n{1}", text, LinkPublic);
				else
					return LinkPublic;
			}
		}

		public void Share()
		{
			var imageFilename = Application.persistentDataPath + "/share.jpg";
			if (image)
				File.WriteAllBytes(imageFilename, image.EncodeToJPG(80));


#if UNITY_ANDROID
      AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
      AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

      intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

      if (image)
        intentObject.Call<AndroidJavaObject>("setType", "image/*");
      else
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");

      intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), ShareText);

      if (image)
      {
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", imageFilename);
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromFile", fileObject);

        bool fileExist = fileObject.Call<bool>("exists");
        Debug.LogFormat("File [{0}] exist [{1}]", imageFilename, fileExist);
        if (fileExist)
          intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
      }
      AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
      currentActivity.Call("startActivity", intentObject);
#else
			Application.OpenURL(string.Format("https://www.facebook.com/sharer/sharer.php?u={0}&quote={1}", LinkLocal, text));
#endif
		}
	}
}