﻿#if UHL_ADS_APPODEAL

using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace UHL
{
  public abstract class AdsBehaviourAppodeal : AdsBehaviourBase, IRewardedVideoAdListener, IInterstitialAdListener
  {
    /// <summary>Your app id from Appodeal Manage Apps page.</summary>
    public string appKey = "9dd271cf80f0a2710be2a82688c8571d2a19497750e37ec5";

    protected int GetAppodealInt(AdType type)
    {
      switch (type)
      {
        case AdType.Interstitial:
          return Appodeal.INTERSTITIAL;
        case AdType.Rewarded:
          return Appodeal.REWARDED_VIDEO;
        default:
          Debug.LogErrorFormat("AdType {0} is not implemented", type.ToString("G"));
          break;
      }
      return Appodeal.INTERSTITIAL;
    }

    protected override void _Initialize()
    {  
      Appodeal.disableLocationPermissionCheck();
      Appodeal.setLogging(debugMode);
      Appodeal.setTesting(debugMode);
      Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO | Appodeal.INTERSTITIAL);
      Appodeal.setRewardedVideoCallbacks(this);
      Appodeal.setInterstitialCallbacks(this);
    }

    protected override bool _IsAvailable(AdType type)
    {
      return Appodeal.isLoaded(GetAppodealInt(type));
    }

    protected override void _Show(AdType type)
    {
      Appodeal.show(GetAppodealInt(type));
    }

    #region IInterstitialAdListener

    public void onInterstitialLoaded()
    {
      onInterstitialReady.Invoke(true);
    }

    public void onInterstitialFailedToLoad()
    {
      onInterstitialReady.Invoke(false);
    }

    public void onInterstitialShown() {}

    public void onInterstitialClicked()
    {
      onInterstitialFinish.Invoke(true);
    }

    public void onInterstitialClosed()
    {
      onInterstitialFinish.Invoke(false);
    }

    #endregion

    #region IRewardedVideoAdListener

    public void onRewardedVideoLoaded()
    {
      onRewardedReady.Invoke(true);
      Debug.Log("onRewardedVideoLoaded");
    }

    public void onRewardedVideoFailedToLoad()
    {
      onRewardedReady.Invoke(false);
      Debug.Log("onRewardedVideoFailedToLoad");
    }

    public void onRewardedVideoShown() {}

    public void onRewardedVideoFinished(int amount, string name)
    {
      onRewardedFinish.Invoke(true);
    }

    public void onRewardedVideoClosed()
    {
      onRewardedFinish.Invoke(false);
    }

    #endregion
  }
}

#endif