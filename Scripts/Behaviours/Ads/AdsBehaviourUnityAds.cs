﻿#if UHL_ADS_UNITY

using UnityEngine.Advertisements;

namespace UHL
{
  public abstract class AdsBehaviourUnityAds : AdsBehaviourBase
  {
    const string placementIdRewardedVideo = "rewardedVideo";

    protected override void _Initialize()
    {
      Advertisement.debugMode = debugMode;
    }

    protected override bool _IsAvailable(AdType type)
    {
      if (type == AdType.Interstitial)
        return Advertisement.IsReady();
      else
        return Advertisement.IsReady(placementIdRewardedVideo);
    }

    protected override void _Show(AdType type)
    {
      var showOptions = new ShowOptions { resultCallback = (ShowResult result) => {
          (type == AdType.Interstitial ? onInterstitialFinish : onRewardedFinish).Invoke(result == ShowResult.Finished)
        }
      };
      if (type == AdType.Interstitial)
        Advertisement.Show(showOptions);
      else
        Advertisement.Show(placementIdRewardedVideo, showOptions);
    }
  }
}

#endif