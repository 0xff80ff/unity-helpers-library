﻿// You can use defines used in #if's below in
// Edit -> Project Settings -> Player -> Other Settings -> Scripting Define Symbols

namespace UHL
{
  #if UHL_ADS_APPODEAL
  public class AdsBehaviour : AdsBehaviourAppodeal {}
  #elif UHL_ADS_UNITY
  public class AdsBehaviour : AdsBehaviourUnityAds {}
  #else
  public class AdsBehaviour : AdsBehaviourMock {}
  #endif
}