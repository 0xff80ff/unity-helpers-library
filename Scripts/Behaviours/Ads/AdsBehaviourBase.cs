﻿using UnityEngine.Events;
using UnityEngine;
using System;

namespace UHL
{
  public abstract class AdsBehaviourBase : UniqueBehaviour<AdsBehaviourBase>
  {
    public enum AdType
    {
      Interstitial,
      Rewarded
    }

    public enum AdToBlock
    {
      All,
      Interstitial,
      Rewarded
    }

    #region PublicInterface

    // TODO: Store in secure format
    const string configKeyRemoveAds = "y6PaTbpekJlaMy";

    /// <summary>For remove ads purchase feature.</summary>
    public bool BlockAds
    {
      get { return PlayerPrefs.HasKey(configKeyRemoveAds); }
      set
      { 
        if (value)
          PlayerPrefs.SetString(configKeyRemoveAds, Convert.ToString(configKeyRemoveAds.GetHashCode(), 2));
        else
          PlayerPrefs.DeleteKey(configKeyRemoveAds);
      }
    }

    public AdToBlock blockAdsAffects;

    /// <summary>Debug mode.</summary>
    public bool debugMode = false;

    /// <summary>Minimal time between interstitials.</summary>
    [Range(0, 600)]
    public float interstitialCoolDownSec = 0;
    float interstitialLastShowTime = 0;

    [Serializable] public class UnityEventBool : UnityEvent<bool> {}

    public UnityEventBool onInterstitialReady;
    public UnityEventBool onInterstitialFinish;
    public UnityEventBool onRewardedReady;
    public UnityEventBool onRewardedFinish;

    /// <summary>Check if advertisement type is blocked.</summary>
    public bool IsBlocked(AdType type)
    {
      switch (blockAdsAffects)
      {
        case AdToBlock.Interstitial:
          return type == AdType.Interstitial && BlockAds;
        case AdToBlock.Rewarded:
          return type == AdType.Rewarded && BlockAds;
        default:
        case AdToBlock.All:
          return BlockAds;
      }
    }

    /// <summary>Check availability of specific advertisement type.</summary>
    public bool IsAvailable(AdType type)
    {
      if (IsBlocked(type))
        return false;
    
      var result = _IsAvailable(type);

      Debug.LogFormat("Advertisment is {0}", result);
      return result;
    }

    /// <summary>Show a specific advertisement type.</summary>
    public void Show(AdType type)
    {
      if (IsBlocked(type))
        return;

      if (type == AdType.Interstitial)
      {
        if (Time.unscaledTime - interstitialLastShowTime < interstitialCoolDownSec)
          return;
        interstitialLastShowTime = Time.unscaledTime;
      }

      _Show(type);
    }

    /// <summary>Show a low revenue advertisement.</summary>
    public void ShowInterstitial()
    {
      Show(AdType.Interstitial);
    }

    /// <summary>Show a high revenue advertisement.</summary>
    public void ShowRewarded()
    {
      Show(AdType.Rewarded);
    }

    #endregion

    #region Implementation

    protected override void Awake()
    {
      try
      {
        base.Awake();
        _Initialize();
        interstitialLastShowTime = Time.unscaledTime;
      }
      catch (DuplicateException e)
      {
        e.Resolve(false);
        return;
      }
    }

    #endregion

    #region ForImplementation

    protected abstract void _Initialize();

    protected abstract bool _IsAvailable(AdType type);

    protected abstract void _Show(AdType type);

    #endregion
  }
}