﻿#if !UHL_ADS_UNITY && !UHL_ADS_APPODEAL

using UnityEngine;

namespace UHL
{
  public abstract class AdsBehaviourMock : AdsBehaviourBase
  {
    protected override void _Initialize()
    {
    }

    protected override bool _IsAvailable(AdType type)
    {
      return Succes;
    }

    protected override void _Show(AdType type)
    {
      bool ok = Succes;
      Debug.LogFormat("Showing {0} Randomly: {1}", type.ToString("G"), ok ? "Sucess" : "Fail");
      if (type == AdType.Interstitial)
        onInterstitialFinish.Invoke(ok);
      else
        onRewardedFinish.Invoke(ok);
    }

    bool Succes { get { return Random.value > Const.GoldenInv; } }
  }
}

#endif