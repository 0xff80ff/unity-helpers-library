﻿using UnityEngine;

namespace UHL
{
	[DefaultExecutionOrder(int.MinValue / 2)]
	public class SingletonBehaviour<T> : MonoBehaviour
		where T : SingletonBehaviour<T>
	{
		public static string assetPath { get { return typeof(T).ToString().Replace('.', '/'); } }

		private static bool applicationQuit = false;
		private static T _instance;
		public static T instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<T>();
					if (!applicationQuit)
					{
						if (_instance == null)
						{
							var prefab = Resources.Load<T>(assetPath);
							if (prefab)
							{
								_instance = Instantiate(prefab);
								Debug.Log(typeof(T).Name + " instanced from resource.");
							}
						}

						if (_instance == null)
						{
							_instance = new GameObject().AddComponent<T>();
							Debug.Log(typeof(T).Name + " instanced from scratch.");
						}

						if (_instance)
							_instance.name = typeof(T).Name;
						else
							Debug.LogError(typeof(T).Name + " cannot be instanced.");
					}
				}
				return _instance;
			}
		}

		protected virtual void Awake()
		{
			if (_instance == null)
			{
				_instance = this as T;
				transform.parent = null;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				Debug.LogWarning(typeof(T).Name + " instance duplication.");
				Destroy(gameObject);
			}
		}

		private void OnApplicationQuit()
		{
			applicationQuit = true;
		}
		
#if UNITY_EDITOR
		protected const string DefaultAssetMenuPath = "Assets/Create/Singleton/";

		protected static string assetPathAbsolute { get { return string.Format("Assets/Resources/{0}.prefab", assetPath); } }

		/// Returns true if new asset was created.
		protected static bool CreateAsset(out T asset)
		{
			asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPathAbsolute);

			if (asset && !UnityEditor.EditorUtility.DisplayDialog("An asset already exists.", "Overwrite it?", "Yes", "No"))
				return false;

			GameObject sceneGameObject = new GameObject(typeof(T).Name, typeof(T));
			GameObject assetGameObject = UnityEditor.PrefabUtility.SaveAsPrefabAsset(sceneGameObject, assetPathAbsolute);
			DestroyImmediate(sceneGameObject);

			asset = assetGameObject.GetComponent<T>();

			UnityEditor.EditorUtility.FocusProjectWindow();

			UnityEditor.Selection.activeObject = assetGameObject;

			return true;
		}
#endif
	}
}