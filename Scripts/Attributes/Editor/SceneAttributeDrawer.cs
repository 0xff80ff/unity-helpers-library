﻿using UnityEditor;
using UnityEngine;

namespace UHL
{
	[CustomPropertyDrawer(typeof(SceneAttribute))]
	public class SceneAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (property.propertyType == SerializedPropertyType.String)
			{
				var sceneObject = GetSceneObject(property.stringValue);
				var scene = EditorGUI.ObjectField(position, label, sceneObject, typeof(SceneAsset), true);
				if (scene == null)
				{
					property.stringValue = string.Empty;
				}
				else if (scene.name != property.stringValue)
				{
					var sceneObj = GetSceneObject(scene.name);
					if (sceneObj)
						property.stringValue = scene.name;
					else
						Debug.LogWarningFormat(
							"The scene {0} cannot be used. To use this scene add it to the build settings for the project",
							scene.name
						);
				}
			}
			else
				EditorGUI.LabelField(position, label.text, "Use [Scene] with strings.");
		}
		protected SceneAsset GetSceneObject(string sceneObjectName)
		{
			if (string.IsNullOrEmpty(sceneObjectName))
				return null;

			foreach (var editorScene in EditorBuildSettings.scenes)
				if (editorScene.path.IndexOf(sceneObjectName) != -1)
					return AssetDatabase.LoadAssetAtPath(editorScene.path, typeof(SceneAsset)) as SceneAsset;

			Debug.LogWarningFormat(
				"Scene [{0}] cannot be used. Add this scene to the 'Scenes in the Build' in build settings.",
				sceneObjectName
			);
			return null;
		}
	}
}
