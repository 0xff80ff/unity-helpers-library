﻿using UnityEngine;
using UnityEditor;

namespace UHL
{
  [CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
  class MinMaxSliderDrawer : PropertyDrawer
  {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
      MinMaxSliderAttribute attr = attribute as MinMaxSliderAttribute;

      float valueWidth = Mathf.Max(EditorGUIUtility.singleLineHeight * 2, EditorGUIUtility.labelWidth * 0.2f);
      Rect r = position;
      r.width = EditorGUIUtility.labelWidth - valueWidth;
      EditorGUI.LabelField(r, label);

      Vector2 lr;
      if (!GetLR(out lr.x, out lr.y, property))
      {
        r.x = EditorGUIUtility.labelWidth;
        r.width = position.width - EditorGUIUtility.labelWidth;
        EditorGUI.LabelField(r, label, "Wrong MinMaxSlider type. Use Vector2.");
        return;
      }

      EditorGUI.BeginChangeCheck();

      r.x += EditorGUIUtility.labelWidth;
      r.width = valueWidth;
      lr.x = EditorGUI.DelayedFloatField(r, lr.x);

      r.x += valueWidth;
      r.width = position.width - EditorGUIUtility.labelWidth - valueWidth * 2;
      EditorGUI.MinMaxSlider(r, ref lr.x, ref lr.y, attr.min, attr.max);

      r.x = position.x + position.width - valueWidth;
      r.width = valueWidth;
      lr.y = EditorGUI.DelayedFloatField(r, lr.y);

      if (EditorGUI.EndChangeCheck())
      {
        if (lr.x > lr.y)
        {
          float tmp = lr.y;
          lr.y = lr.x;
          lr.x = tmp;
        }
        if (!SetLR(lr.x, lr.y, property))
          Debug.LogErrorFormat("MinMaxSliderAttribute has getter for {0} but has not setter", property.propertyType);
      }
    }

    bool GetLR(out float left, out float right, SerializedProperty property)
    {
      if (property.propertyType == SerializedPropertyType.Vector2)
      {
        left = property.vector2Value.x;
        right = property.vector2Value.y;
        return true;
      }

      left = (attribute as MinMaxSliderAttribute).min;
      right = (attribute as MinMaxSliderAttribute).max;
      return false;
    }

    bool SetLR(float left, float right, SerializedProperty property)
    {
      if (property.propertyType == SerializedPropertyType.Vector2)
      {
        property.vector2Value = new Vector2(left, right);
        return true;
      }

      return false;
    }
  }
}
