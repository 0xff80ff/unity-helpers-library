﻿using UnityEngine;

namespace UHL
{
	public class ReadOnlyAttribute : PropertyAttribute { }
}
