using System.Collections.Generic;
using UnityEngine;

namespace UHL
{
	public static class Extensions
	{
		internal static int Mod(this int self, int period)
		{
			return ((self % period) + period) % period;
		}

		internal static int GetStableHashCode(this string str)
		{
			unchecked
			{
				int hash1 = 5381;
				int hash2 = hash1;

				for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
				{
					hash1 = ((hash1 << 5) + hash1) ^ str[i];
					if (i == str.Length - 1 || str[i + 1] == '\0')
						break;
					hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
				}

				return hash1 + (hash2 * 1566083941);
			}
		}

		internal static T GetRandom<T>(this IList<T> self)
		{
			return self.Count == 0 ? default(T) : self[Random.Range(0, self.Count)];
		}

		#region Float

		internal static float BackOut(this float t, float force = 4)
		{
			float tt = t * t;
			float ttt = tt * t;
			return (2 + force) * t - (1 + 2 * force) * tt + force * ttt;
		}

		internal static float LogCut(this float t, float threshold)
		{
			return t > threshold
				? threshold * Mathf.Log(t) + (1 + Mathf.Log(threshold)) * threshold
				: t;
		}

		#endregion

		#region Geometry

		internal static Vector3 GetNearestLineSegmentPoint(this Vector3 point, Vector3 lineStart, Vector3 lineFinish)
		{
			Vector3 lineDirection = lineFinish - lineStart;
			float lineLengthSq = lineDirection.sqrMagnitude;
			if (lineLengthSq < Const.Tiny)
				return lineStart;

			float projectionCoord = Mathf.Clamp01(Vector3.Dot(point - lineStart, lineDirection) / lineLengthSq);
			Vector3 projection = lineStart + lineDirection * projectionCoord;
			return projection;
		}

		internal static float GetDistanceToLineSegment(this Vector3 point, Vector3 lineStart, Vector3 lineFinish)
		{
			return (point - point.GetNearestLineSegmentPoint(lineStart, lineFinish)).magnitude;
		}

		internal static bool IntersectWithLineSegment(this Ray2D ray, Vector2 lineStart, Vector2 lineFinish, out Vector2 intersection, bool onlyPositiveSide = true)
		{
			Vector2 lineDirection = lineFinish - lineStart;
			Vector2 conjugate = new Vector2(lineDirection.y, -lineDirection.x);

			float numerator = Vector2.Dot(lineStart - ray.origin, conjugate);
			float denominator = Vector2.Dot(ray.direction, conjugate);

			if (Mathf.Abs(denominator) > Const.Tiny)
			{
				float rayRatio = numerator / denominator;
				if (rayRatio >= 0 || !onlyPositiveSide)
				{
					intersection = ray.origin + ray.direction * rayRatio;
					float projectionCoord = Vector3.Dot(intersection - lineStart, lineDirection) / lineDirection.sqrMagnitude;
					return 0 <= projectionCoord && projectionCoord <= 1;
				}
			}
			else if (Mathf.Abs(numerator) < Const.Tiny)
			{
				intersection = (ray.origin - lineStart).sqrMagnitude < (ray.origin - lineFinish).sqrMagnitude ? lineStart : lineFinish;
				return true;
			}

			intersection = Vector2.positiveInfinity;
			return false;
		}

		internal static bool IntersectWithPlane(this Ray ray, Vector3 planeOrigin, Vector3 planeNormal, out Vector3 intersection)
		{
			float overlaping = Vector3.Dot(planeOrigin - ray.origin, planeNormal);
			float nonParallelity = Vector3.Dot(ray.direction, planeNormal);
			if (Mathf.Abs(nonParallelity) < Const.Tiny)
			{
				intersection = ray.origin;
				return Mathf.Abs(overlaping) < Const.Tiny;
			}

			intersection = ray.origin + ray.direction * overlaping / nonParallelity;

			return overlaping >= 0;
		}

		internal static bool IntersectWithTriangle(this Ray ray, Vector3 v0, Vector3 v1, Vector3 v2, out float distance)
		{
			Vector3 e1 = v1 - v0;
			Vector3 e2 = v2 - v0;

			Vector3 pvec = Vector3.Cross(ray.direction, e2);
			float det = Vector3.Dot(e1, pvec);

			if (Mathf.Abs(det) < Const.Tiny) // Parallel
			{
				distance = float.MaxValue;
				return false;
			}

			float inv_det = 1 / det;
			Vector3 tvec = ray.origin - v0;
			float u = Vector3.Dot(tvec, pvec) * inv_det;
			if (u < 0 || 1 < u)
			{
				distance = float.MaxValue;
				return false;
			}

			Vector3 qvec = Vector3.Cross(tvec, e1);
			float v = Vector3.Dot(ray.direction, qvec) * inv_det;
			if (v < 0 || 1 < u + v)
			{
				distance = float.MaxValue;
				return false;
			}

			distance = Vector3.Dot(e2, qvec) * inv_det;
			return true;
		}

		#endregion

		#region UnityObjects

		internal static T Find<T>(this Transform self, string name)
			where T : class
		{
			var result = self.Find(name);
			return result ? result.GetComponent<T>() : null;
		}

		internal static Transform FindRecursive(this Transform self, string name)
		{
			var queue = new Queue<Transform>();
			queue.Enqueue(self);

			while (queue.Count > 0)
			{
				var cur = queue.Dequeue();
				var result = cur.Find(name);
				if (result)
					return result;

				foreach (Transform child in cur)
					queue.Enqueue(child);
			}
			return null;
		}

		internal static void SetNamePostfix(this Object self, string postfix)
		{
			string justName = self.name;

			if (self.name.EndsWith(")"))
			{
				int postfixStartIndex = self.name.LastIndexOf('(');
				if (postfixStartIndex > 0)
				{
					justName = self.name.Substring(0, postfixStartIndex);
				}
			}

			self.name = string.Format("{0}({1})", justName, postfix);
		}

		#endregion
	}
}
