﻿using UnityEngine;
using UnityEngine.UI;

namespace UHL
{
  [RequireComponent(typeof(Text))]
  [AddComponentMenu("UHL/AutoTranslatorUIText")]
  public class AutoTranslatorUIText : AutoTranslatorBase
  {
    Text text;

    protected override string Text
    {
      get { return text.text; }
      set { text.text = value; }
    }

    protected override void Awake()
    {
      text = GetComponent<Text>();
      base.Awake();
    }
  }
}
