﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace UHL
{
  [CustomEditor(typeof(Translator))]
  public class TranslatorDrawer : Editor
  {
    bool showPhrases = false;

    public override void OnInspectorGUI()
    {
      serializedObject.Update();

      EditorGUI.BeginChangeCheck();

      EditorGUILayout.PropertyField(serializedObject.FindProperty("translationsFile"));
      EditorGUILayout.PropertyField(serializedObject.FindProperty("caseSensitiveIds"));

      serializedObject.ApplyModifiedProperties();

      if (EditorGUI.EndChangeCheck())
        CallPrivateMethod("LoadPhrases");

      var newDefaultLang = (SystemLanguage)EditorGUILayout.EnumPopup("Default Language:", Translator.DefaultLanguage);
      if (Translator.DefaultLanguage != newDefaultLang)
        Translator.DefaultLanguage = newDefaultLang;
      
      var newSimulateLang = (SystemLanguage)EditorGUILayout.EnumPopup("Simulate Language:", Translator.Language);
      if (Translator.Language != newSimulateLang)
        Translator.Language = newSimulateLang;
      
      var phrases = GetPrivateField<Dictionary<string, string>>("phrases");
      if (phrases == null)
        return;
      
      if (showPhrases = EditorGUILayout.Foldout(showPhrases, string.Format("{0} phrases loaded:", phrases.Count)))
        foreach (var kv in phrases)
        {
          var rect = EditorGUILayout.GetControlRect();
          rect.width *= 0.5f;
          EditorGUI.SelectableLabel(rect, kv.Key);
          rect.x += rect.width;
          EditorGUI.SelectableLabel(rect, kv.Value);
        }
    }

    FieldType GetPrivateField<FieldType>(string name) where FieldType : class
    {
      var fieldRef = typeof(Translator).GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);
      if (fieldRef == null)
        return null;
      return fieldRef.GetValue(target) as FieldType;
    }

    void CallPrivateMethod(string name)
    {
      var methodRef = typeof(Translator).GetMethod(name, BindingFlags.NonPublic | BindingFlags.Instance);
      if (methodRef == null)
        return;
      methodRef.Invoke(target, null);
    }
  }
}