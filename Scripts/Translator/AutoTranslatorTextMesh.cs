﻿using UnityEngine;

namespace UHL
{
  [RequireComponent(typeof(TextMesh))]
  [AddComponentMenu("UHL/AutoTranslatorTextMesh")]
  public class AutoTranslatorTextMesh : AutoTranslatorBase
  {
    TextMesh text;

    protected override string Text
    {
      get { return text.text; }
      set { text.text = value; }
    }

    protected override void Awake()
    {
      text = GetComponent<TextMesh>();
      base.Awake();
    }
  }
}
