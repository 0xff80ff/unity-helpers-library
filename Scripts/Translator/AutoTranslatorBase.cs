﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Text.RegularExpressions;

namespace UHL
{
	public abstract class AutoTranslatorBase : MonoBehaviour
	{
		#region Properties

		public enum TranslateUnit
		{
			WholeText,
			BetweenDollarCurlyBraces,
			BetweenPercents
		}

		string _originalText;

		public string OriginalText
		{
			get { return _originalText; }
			set
			{
				_originalText = value;
				TranslateText();
			}
		}

		public TranslateUnit translateUnit = TranslateUnit.WholeText;

		public UnityEvent OnTranslated;

		#endregion

		#region Implementation

		protected virtual void Awake()
		{
			OriginalText = Text;
			TranslateText();
			Translator.onLanguageChange.AddListener(OnLanguageChange);
		}

		protected virtual void OnDestroy()
		{
			Translator.onLanguageChange.RemoveListener(OnLanguageChange);
		}

		void OnLanguageChange(SystemLanguage lang)
		{
			TranslateText();
		}

		protected void TranslateText()
		{
			switch (translateUnit)
			{
				case TranslateUnit.WholeText:
					Text = Translator.Translate(OriginalText);
					break;
				case TranslateUnit.BetweenDollarCurlyBraces:
					TranslateRegexGroup(@"\$\{(.+?)\}");
					break;
				case TranslateUnit.BetweenPercents:
					TranslateRegexGroup(@"%(.+?)%");
					break;
				default:
					Debug.LogErrorFormat("Translate Unit '{0}' is not implemented.", translateUnit.ToString("g"));
					break;
			}
			if (OnTranslated != null)
				OnTranslated.Invoke();
		}

		protected void TranslateRegexGroup(string patternWithGroup)
		{
			Text =
			  Regex.Replace(OriginalText, patternWithGroup, (Match match) => Translator.Translate(match.Groups[match.Groups.Count - 1].Value));
		}

		#endregion

		#region ForImplementation

		protected abstract string Text { get; set; }

		#endregion
	}
}