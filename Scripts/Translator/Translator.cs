﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;
using UnityEngine.Events;

namespace UHL
{
	[ExecuteInEditMode]
	public class Translator : SingletonScriptableObject<Translator>
	{
		#region FieldsAndProperties

		const string languagePropertyID = "UHL_Language";

		[SerializeField]
		TextAsset translationsFile;

		public bool caseSensitiveIds = true;

		private Dictionary<string, string> phrases;

		#endregion

		#region PublicInterface

		/// <summary> Calls after every change of Language property. </summary>
		public static readonly UnityEvent<SystemLanguage> onLanguageChange = new UnityEventSystemLanguage();

		/// <summary> Translation target Language. </summary>
		public static SystemLanguage Language
		{
			get
			{
				SystemLanguage lang;
				try
				{
					lang = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), PlayerPrefs.GetString(languagePropertyID));
					if (lang == SystemLanguage.Unknown)
						lang = Application.systemLanguage;
				}
				catch
				{
					lang = Application.systemLanguage;
				}
				return lang;
			}
			set
			{
				if (!Enum.IsDefined(typeof(SystemLanguage), value) || value == SystemLanguage.Unknown)
					value = DefaultLanguage;
				PlayerPrefs.SetString(languagePropertyID, Enum.GetName(typeof(SystemLanguage), value));
				instance.LoadPhrases();
				onLanguageChange.Invoke(value);
			}
		}

		public static SystemLanguage DefaultLanguage
		{
			get { return instance == null ? Application.systemLanguage : instance._defaultTranslationLanguage; }
			set
			{
				if (instance != null)
				{
					instance._defaultTranslationLanguage =
					(!Enum.IsDefined(typeof(SystemLanguage), value) || value == SystemLanguage.Unknown)
					? Application.systemLanguage
					: value;
					instance.LoadPhrases();
				}
			}
		}

		[SerializeField]
		private SystemLanguage _defaultTranslationLanguage = SystemLanguage.Unknown;

		/// <returns> Translation for phraseID if found, phraseID otherwise. </returns>
		public static string Translate(string phraseID)
		{
			string translation;
			return instance.phrases.TryGetValue(phraseID, out translation) ? translation : phraseID;
		}

		/// <summary> Puts translation to output argument if found. </summary>
		/// <returns> True if phraseID found for current Language. </returns>
		public static bool TryTranslate(string phraseID, out string translation)
		{
			return instance.phrases.TryGetValue(phraseID, out translation);
		}

		/// <returns> True if phraseID found for current Language. </returns>
		public static bool CanTranslate(string phraseID)
		{
			return instance.phrases.ContainsKey(phraseID);
		}

		#endregion

		#region Implementation

		private class UnityEventSystemLanguage : UnityEvent<SystemLanguage> { }

		protected virtual void OnEnable()
		{
			DefaultLanguage = DefaultLanguage;
			LoadPhrases();
		}

		protected void LoadPhrases()
		{
			//Debug.Log("LoadPhrases");
			var targetLanguage = Language;
			var ifNotFoundLanguage = DefaultLanguage;
			try
			{
				if (phrases == null)
					phrases = new Dictionary<string, string>(caseSensitiveIds ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase);
				else
					phrases.Clear();

				if (translationsFile == null)
					return;

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(translationsFile.text);

				var root = doc.DocumentElement;

				foreach (XmlNode phrase in root.GetElementsByTagName("phrase"))
				{
					var phraseId = phrase.Attributes["id"].Value;

					foreach (XmlNode translation in phrase.ChildNodes)
					{
						var curLang = ParseSystemLanguage(translation.Name);
						if (curLang == targetLanguage)
						{
							phrases[phraseId] = translation.InnerXml;
							break;
						}
						if (curLang == ifNotFoundLanguage)
						{
							phrases.Add(phraseId, translation.InnerXml);
							continue;
						}
					}
				}
			}
			catch (Exception e)
			{
				Debug.LogError("Translations are not loaded: " + e.Message);
			}
		}

		protected SystemLanguage ParseSystemLanguage(string languageName)
		{
			SystemLanguage result;
			try
			{
				result = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), languageName, true);
			}
			catch
			{
				Debug.LogError("Unknown Language " + languageName);
				result = SystemLanguage.Unknown;
			}
			return result;
		}

		#endregion

#if UNITY_EDITOR
		[UnityEditor.MenuItem(DefaultAssetMenuPath + "Translator")]
		static private void CreateAsset()
		{
			Translator t;
			CreateAsset(out t);
		}
#endif
	}
}