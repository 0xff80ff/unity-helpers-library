﻿using UnityEngine;
using System.Collections.Generic;

namespace UHL
{
  [System.Serializable]
  public class Spline
  {
    public List<Vector3> points;

    public Vector3 At(float t)
    {
      Vector3 leftPoint, middlePoint, rightPoint;
      UniformToLocal(ref t, out leftPoint, out middlePoint, out rightPoint);

      return Vector3.Lerp(Vector3.Lerp(leftPoint, middlePoint, t), Vector3.Lerp(middlePoint, rightPoint, t), t);
    }

    public Vector3 DirectionAt(float t)
    {
      Vector3 leftPoint, middlePoint, rightPoint;
      UniformToLocal(ref t, out leftPoint, out middlePoint, out rightPoint);

      return (leftPoint * (t - 1) + middlePoint * (1 - 2 * t) + rightPoint * t) * 2;
    }

    void UniformToLocal(ref float t, out Vector3 prev, out Vector3 cur, out Vector3 next)
    {
      int lastPoint = points.Count - 1;
      int preLastPoint = lastPoint - 1;
      float scaledT = preLastPoint * t;
      int prevIndex = Mathf.Min(preLastPoint - 1, Mathf.FloorToInt(scaledT));
      int curIndex = Mathf.Min(preLastPoint, prevIndex + 1);
      int nextIndex = Mathf.Min(lastPoint, prevIndex + 2);
      t = (scaledT - prevIndex);

      cur = points[curIndex];
      prev = prevIndex == 0 ? points[prevIndex] : ((points[prevIndex] + cur) * 0.5f);
      next = nextIndex == points.Count - 1 ? points[nextIndex] : ((points[nextIndex] + cur) * 0.5f);
    }

    public void DrawGizmos(Transform transform)
    {
      float dt = 0.125f / points.Count;
      for (float t = dt; t <= 1; t += dt)
      {
        var start = transform.TransformPoint(At(t - dt));
        var end = transform.TransformPoint(At(t));
        Gizmos.DrawLine(start, end);
                
        /*var dir = transform.TransformDirection(DirectionAt(t)).normalized;
                Gizmos.DrawLine(start, start + dir);*/
      }

      var color = Gizmos.color;
      color.a *= 0.5f;
      Gizmos.color = color;
            
      for (int i = 1; i < points.Count; ++i)
        Gizmos.DrawLine(transform.TransformPoint(points[i - 1]), transform.TransformPoint(points[i]));

      color.a *= 2;
      Gizmos.color = color;
    }
  }
}
