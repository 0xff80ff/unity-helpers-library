﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace UHL
{
    [CustomEditor(typeof(Loft))]
    public class LoftEditor : Editor
    {
        private void OnSceneGUI()
        {
            Loft loft = target as Loft;

            Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ? loft.transform.rotation : Quaternion.identity;
            Handles.color = Color.white;
            
            for (int i = 0; i < loft.path.points.Count; ++i)
            {
                var point = loft.transform.TransformPoint(loft.path.points[i]);
                EditorGUI.BeginChangeCheck();
                var editedPoint = Handles.DoPositionHandle(point, handleRotation);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(loft, "Move Point");
                    EditorUtility.SetDirty(loft);
                    loft.path.points[i] = loft.transform.InverseTransformPoint(editedPoint);
                }
            }
        }
    }
}