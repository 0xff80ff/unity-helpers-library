﻿using UnityEngine;
using System.Collections.Generic;

namespace UHL
{
  public class Loft : MonoBehaviour
  {
    public enum Axis
    {
      X,
      Y,
      Z
    }

    public enum ProcessEvent
    {
      Awake,
      Enable,
      FromScript
    }

    public enum Algorithm
    {
      ByCount,
      StrightParts
    }

    public ProcessEvent processEvent;
    public Mesh mesh;
    public Axis meshAxis;
    public Spline path;
    public bool merge = true;

    public Algorithm algorithm = Algorithm.ByCount;

    [Header("ByCount")]

    [Range(1, 1000)]
    public int count = 1;

    [Header("StrightParts")]

    [Range(0.01f, 10)]
    public float threshold = 5;

    public List<Mesh> output = new List<Mesh>();

    void OnDrawGizmosSelected()
    {
      Gizmos.color = Color.cyan;
      path.DrawGizmos(transform);
    }

    void OnEnable()
    {
      if (processEvent == ProcessEvent.Enable)
        Process();
    }

    void Awake()
    {
      if (processEvent == ProcessEvent.Awake)
        Process();
    }

    public void Process()
    {
      Vector3 axis;
      System.Func<Vector3, float> getAxis;
      System.Func<Vector3, Vector3> avoidAxis;

      if (meshAxis == Axis.X)
      {
        axis = Vector3.right;
        getAxis = (Vector3 v) => v.x;
        avoidAxis = (Vector3 v) => new Vector3(0, v.y, v.z);
      }
      else if (meshAxis == Axis.Y)
      {
        axis = Vector3.up;
        getAxis = (Vector3 v) => v.y;
        avoidAxis = (Vector3 v) => new Vector3(v.x, 0, v.z);
      }
      else
      {
        axis = Vector3.forward;
        getAxis = (Vector3 v) => v.z;
        avoidAxis = (Vector3 v) => new Vector3(v.x, v.y, 0);
      }

      float minAxis = float.MaxValue;
      float maxAxis = float.MinValue;

      foreach (var vert in mesh.vertices)
      {
        float axisVal = getAxis(vert);
        if (axisVal < minAxis)
          minAxis = axisVal;
        if (axisVal > maxAxis)
          maxAxis = axisVal;
      }
      float axisRange = maxAxis - minAxis;

      Mesh outMesh = null;
      List<Vector3> outVertices = null;
      List<Vector3> outNormals = null;
      List<Vector2> outUVs = null;
      List<int> outTriangles = null;

      System.Action startOutMesh = () =>
      {
        output.Add(outMesh = new Mesh());
        outVertices = new List<Vector3>(mesh.vertexCount * count);
        outNormals = new List<Vector3>(mesh.normals.Length * count);
        outUVs = new List<Vector2>(mesh.uv.Length * count);
        outTriangles = new List<int>(mesh.triangles.Length * count);
      };

      System.Action finishOutMesh = () =>
      {
        if (outMesh == null)
          return;
        
        outMesh.vertices = outVertices.ToArray();
        outMesh.normals = outNormals.ToArray();
        outMesh.uv = outUVs.ToArray();
        outMesh.triangles = outTriangles.ToArray();
      };

      System.Action appendToOutMesh = () =>
      {
        int indexShift = outVertices.Count;
        outVertices.AddRange(mesh.vertices);
        outNormals.AddRange(mesh.normals);
        outUVs.AddRange(mesh.uv);
        int indexToShift = outTriangles.Count;
        outTriangles.AddRange(mesh.triangles);
        do
        {
          outTriangles[indexToShift] += indexShift;
        }
        while (++indexToShift < outTriangles.Count);
      };

      output.Clear();
      if (merge)
        startOutMesh();

      System.Action<float, float> placeMesh = (float tStart, float tFinish) =>
      {
        if (!merge)
        {
          finishOutMesh();
          startOutMesh();
        }

        int vertexToTransform = outVertices.Count;
        appendToOutMesh();
        for (int j = vertexToTransform; j < outVertices.Count; ++j)
        {
          var vertex = outVertices[j];

          float t = Mathf.Lerp(tStart, tFinish, (getAxis(vertex) - minAxis) / axisRange);

          //Quaternion rotation = Quaternion.FromToRotation(axis, path.DirectionAt(t));
          Quaternion rotation = Quaternion.LookRotation(path.DirectionAt(t));

          vertex = avoidAxis(vertex);
          vertex = rotation * vertex;
          vertex += path.At(t);

          outVertices[j] = vertex;
        }
      };

      switch (algorithm)
      {
        case Algorithm.ByCount:
          {
            float dt = 1.0f / count;
            for (float t = 0; t <= 0.999f; t += dt)
              placeMesh(t, t + dt);
          }
          break;
        case Algorithm.StrightParts:
          {
            float a = 0;
            Vector3 posA = path.At(a);
            Vector3 dirA = path.DirectionAt(a).normalized;
            float dt = 0.125f / path.points.Count;
            for (float b = dt; b <= 1; b += dt)
            {
              Vector3 posB = path.At(b);
              float deltaPosLength = (posB - posA).magnitude;
              Vector3 strengthPosB = posA + dirA * deltaPosLength;
              if ((strengthPosB - posB).magnitude > threshold)
              {
                placeMesh(a, b);
                a = b;
                posA = path.At(b);
                dirA = path.DirectionAt(b).normalized;
              }
            }
            if (a < 1)
              placeMesh(a, 1);
          }
          break;
      }

      finishOutMesh();
    }
  }
}