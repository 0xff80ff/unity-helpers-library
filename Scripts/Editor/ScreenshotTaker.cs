﻿using UnityEngine;
using UnityEditor;

public class ScreenshotTaker : EditorWindow
{
  [MenuItem("Tools/ScreenshotTaker")]
  public static void TakeScreenshot()
  {
    EditorWindow.GetWindow(typeof(ScreenshotTaker));
  }

  void OnGUI()
  {
    if (GUILayout.Button("Screenshot x1"))
      Capture(1);

    if (GUILayout.Button("Screenshot x2"))
      Capture(2);

    if (GUILayout.Button("Screenshot x4"))
      Capture(4);

    if (GUILayout.Button("Screenshot x8"))
      Capture(8);
  }

  void Capture(int ratio)
  {
    if (!System.IO.Directory.Exists("Screenshots"))
      System.IO.Directory.CreateDirectory("Screenshots");
    ScreenCapture.CaptureScreenshot(string.Format("Screenshots/{0}.jpg", System.DateTime.Now.ToBinary()), ratio);
  }
}
