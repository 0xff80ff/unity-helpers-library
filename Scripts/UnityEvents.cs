﻿using UnityEngine.Events;
using UnityEngine;

namespace UHL
{
	[System.Serializable]
	public class UnityEventBool : UnityEvent<bool> { }

	[System.Serializable]
	public class UnityEventInt : UnityEvent<int> { }

	[System.Serializable]
	public class UnityEventFloat : UnityEvent<float> { }

	[System.Serializable]
	public class UnityEventVector2 : UnityEvent<Vector2> { }

	[System.Serializable]
	public class UnityEventVector3 : UnityEvent<Vector3> { }

	[System.Serializable]
	public class UnityEventString : UnityEvent<string> { }
}
