﻿using UnityEngine;
using UnityEngine.UI;

namespace UHL
{
  [RequireComponent(typeof(Button))]
  public class ButtonHardware : MonoBehaviour
  {
    public KeyCode key;

    Button button;

    void Awake()
    {
      button = GetComponent<Button>();
    }

    void Update()
    {
      if (Input.GetKeyDown(key) && button.IsInteractable())
        button.onClick.Invoke();
  	}
  }
}