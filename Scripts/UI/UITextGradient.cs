﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

namespace UHL
{
  [AddComponentMenu("UI/Effects/TextGradient")]
  public class UITextGradient : BaseMeshEffect
  {
    public enum Style
    {
      Horizontal,
      Vertical,
      Diagonal,
      Codiagonal,
      Radial,
      XYRadius2RGB
    }

    public Style style;
    public Gradient gradient;

    public override void ModifyMesh(VertexHelper vh)
    {
      List<UIVertex> verts = new List<UIVertex>();
      vh.GetUIVertexStream(verts);

      ModifyVerts(ref verts);

      vh.AddUIVertexTriangleStream(verts);
    }

    void ModifyVerts(ref List<UIVertex> verts)
    {
      if (!IsActive() || verts.Count == 0)
        return;
    
      Vector2 min = (Vector2)verts.Select((UIVertex v) => v.position).Aggregate(Vector3.Min);
      Vector2 max = (Vector2)verts.Select((UIVertex v) => v.position).Aggregate(Vector3.Max);
      Vector2 normalizer = Vector2.Max(Vector2.one, max - min);
      normalizer.x = 1 / normalizer.x;
      normalizer.y = 1 / normalizer.y;

      System.Func<Vector3, Color> positionToColor;
      if (style == Style.Horizontal)
        positionToColor = (Vector3 p) => gradient.Evaluate((p.x - min.x) * normalizer.x);
      else if (style == Style.Vertical)
        positionToColor = (Vector3 p) => gradient.Evaluate((p.y - min.y) * normalizer.y);
      else if (style == Style.Diagonal)
        positionToColor = (Vector3 p) =>
        {
          Vector2 delta = Vector2.Scale((Vector2)p - min, normalizer);
          return gradient.Evaluate((delta.x + delta.y) * 0.5f);
        };
      else if (style == Style.Codiagonal)
        positionToColor = (Vector3 p) =>
        {
          Vector2 delta = Vector2.Scale((Vector2)p - min, normalizer);
          return gradient.Evaluate((1 + delta.x - delta.y) * 0.5f);
        };
      else if (style == Style.Radial)
        positionToColor = (Vector3 p) =>
        {
          Vector2 delta = Vector2.Scale((Vector2)p - min, normalizer);
          return gradient.Evaluate(Vector2.Distance(delta * 2, Vector2.one));
        };
      else
        positionToColor = (Vector3 p) =>
        {
          Vector2 delta = Vector2.Scale((Vector2)p - min, normalizer);
          return new Color(delta.x, delta.y, Vector2.Distance(delta * 2, Vector2.one), 1);
        };

      for (int i = 0; i < verts.Count; ++i)
      {
        var vert = verts[i];
        vert.color = positionToColor(vert.position);
        verts[i] = vert;
      }
    }
  }
}