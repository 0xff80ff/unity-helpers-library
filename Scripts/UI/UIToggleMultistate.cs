﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UHL
{
	public class UIToggleMultistate : Selectable
	{
		public Graphic[] stateGraphics;

		[SerializeField]
		int _currentState = 0;

		public int CurrentState
		{
			get { return _currentState; }
			set { onStateChange.Invoke(_currentState = Mathf.Clamp(value, 0, stateGraphics.Length - 1)); }
		}

		public bool inverseOrder = false;
		public bool pingPong = false;

		[System.Serializable]
		public class UnityEventInt : UnityEvent<int> { }

		public UnityEventInt onStateChange;

		protected override void Awake()
		{
			base.Awake();

			onStateChange.AddListener(SwitchGraphic);
			CurrentState = CurrentState;

			if (stateGraphics.Length < 2)
			{
				enabled = false;
				Debug.Assert(stateGraphics.Length > 0, "There is no states. But must be at least 2.");
			}
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			onStateChange.RemoveListener(SwitchGraphic);
		}

		public override void OnPointerUp(UnityEngine.EventSystems.PointerEventData eventData)
		{
			base.OnPointerUp(eventData);

			int nextState = CurrentState + (inverseOrder ? -1 : 1);
			if (nextState < 0)
			{
				if (pingPong)
				{
					inverseOrder = false;
					nextState = 1;
				}
				else
					nextState = stateGraphics.Length - 1;
			}
			else if (nextState >= stateGraphics.Length)
			{
				if (pingPong)
				{
					inverseOrder = true;
					nextState = stateGraphics.Length - 2;
				}
				else
					nextState = 0;
			}

			CurrentState = nextState;
		}

		void SwitchGraphic(int newState)
		{
			for (int i = 0; i < stateGraphics.Length; ++i)
				if (stateGraphics[i] != null)
					stateGraphics[i].gameObject.SetActive(i == newState);
		}
	}
}