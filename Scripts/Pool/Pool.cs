﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UHL
{
	public class Pool : SingletonBehaviour<Pool>
	{
		[System.Serializable]
		public class PoolElement
		{
			public GameObject prefab;
			[Range(1, 10000)]
			public int count = 10;
			public bool addOver = true;
			[System.NonSerialized]
			public readonly LinkedList<GameObject> instances = new LinkedList<GameObject>();
		}

		public PoolElement[] prefabs;

		Dictionary<string, PoolElement> namedPrefabs = new Dictionary<string, PoolElement>();

		protected override void Awake()
		{
			base.Awake();

			foreach (var pe in prefabs)
			{
				Debug.AssertFormat(pe.count > 1, "Too few of [{0}]", pe.prefab.name);
				Debug.AssertFormat(!namedPrefabs.ContainsKey(pe.prefab.name), "Duplication of [{0}]", pe.prefab.name);

				namedPrefabs.Add(pe.prefab.name, pe);
				for (int i = 0; i < pe.count; ++i)
					pe.instances.AddLast(UHL.SingletonBehaviour<Pool>.Instantiate(pe.prefab, transform)).Value.SetActive(false);
			}
		}

		public static GameObject Spawn(GameObject prefab, Transform parent = null)
		{
			PoolElement pe;
			if (instance == null || !instance.namedPrefabs.TryGetValue(prefab.name, out pe))
				return null;

			if (pe.instances.First.Value.activeSelf && pe.addOver)
				pe.instances.AddFirst(UHL.SingletonBehaviour<Pool>.Instantiate(pe.prefab, instance.transform));

			var nextInstance = pe.instances.First.Value;
			pe.instances.RemoveFirst();
			pe.instances.AddLast(nextInstance);

			nextInstance.transform.parent = parent;
			nextInstance.SetActive(true);
			nextInstance.SendMessage("Start", SendMessageOptions.DontRequireReceiver);
			return nextInstance;
		}

		public T Spawn<T>(GameObject prefab, Transform parent = null)
		  where T : MonoBehaviour
		{
			return Spawn(prefab, parent).GetComponent<T>();
		}

		public static void Despawn(GameObject obj)
		{
			obj.SetActive(false);
			if (instance != null)
				obj.transform.parent = instance.transform;
		}
		
#if UNITY_EDITOR
		[UnityEditor.MenuItem(DefaultAssetMenuPath + "Pool")]
		static private void CreateAsset()
		{
			Pool p;
			CreateAsset(out p);
		}
#endif
	}
}