﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UHL
{
  public class PoolAutoDespawn : MonoBehaviour
  {
    [Range(0, 1000)]
    public float delay = 0;

    protected void Awake()
    {
      if (delay > 0)
        Invoke("Despawn", delay);
    }

    public void Despawn()
    {
      Pool.Despawn(gameObject);
  	}
  }

}