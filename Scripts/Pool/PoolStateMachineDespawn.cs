﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UHL
{
  public class PoolStateMachineDespawn : StateMachineBehaviour
  {
    public enum Event
    {
      MachineExit,
      StateEnter,
      StateExit
    }

    public enum DespawnTarget
    {
      This = 0,
      Parent = 1,
      ParentOfParent = 2
    }

    public Event despawnEvent = Event.MachineExit;
    public DespawnTarget despawnTarget = DespawnTarget.This;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      if (despawnEvent == Event.StateEnter)
        Despawn(animator.transform);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      if (despawnEvent == Event.StateExit)
        Despawn(animator.transform);
    }

    public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
      if (despawnEvent == Event.MachineExit)
        Despawn(animator.transform);
    }

    public void Despawn(Transform self)
    {
      Pool.Despawn(
        despawnTarget == DespawnTarget.This ? self.gameObject : 
        despawnTarget == DespawnTarget.Parent ? self.parent.gameObject :
        /*spawnTarget == DespawnTarget.ParentOfParent*/ self.parent.parent.gameObject
      );
    }
  }
}
