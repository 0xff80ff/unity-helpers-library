﻿using UnityEngine;
using System.Collections.Generic;

namespace UHL
{
	public abstract class TouchOneByOne : MonoBehaviour
	{
		public bool multiTouch = true;

		protected virtual void Awake()
		{
			Input.multiTouchEnabled = multiTouch;
		}

		protected virtual void Update()
		{
			for (int i = 0; i < Input.touchCount; ++i)
				ProcessTouch(Input.GetTouch(i));

#if UNITY_STANDALONE || UNITY_EDITOR
			if (Input.touchCount > 0)
				return;

			if (Input.GetMouseButtonDown(0))
				ProcessTouch(new Touch() { position = Input.mousePosition, phase = TouchPhase.Began, pressure = 1, fingerId = -1 });
			else if (Input.GetMouseButtonUp(0))
				ProcessTouch(new Touch() { position = Input.mousePosition, phase = TouchPhase.Ended, pressure = 1, fingerId = -1 });
			else if (Input.GetMouseButton(0))
				ProcessTouch(new Touch() { position = Input.mousePosition, phase = TouchPhase.Moved, pressure = 1, fingerId = -1 });
#elif UNITY_ANDROID
			if (AndroidInput.secondaryTouchEnabled)
				for (int i = 0; i < AndroidInput.touchCountSecondary; ++i)
					ProcessTouch(AndroidInput.GetSecondaryTouch(i));
#endif
		}

		protected abstract void ProcessTouch(Touch t);

		public static Vector3 ScreenToXZ(Vector2 pos, float originY = 0)
		{
			Ray touchRay = Camera.main.ScreenPointToRay(pos);
			return touchRay.origin + touchRay.direction * ((originY - touchRay.origin.y) / touchRay.direction.y);
		}

		public static Vector3 ScreenToXY(Vector2 pos, float originZ = 0)
		{
			Ray touchRay = Camera.main.ScreenPointToRay(pos);
			return touchRay.origin + touchRay.direction * ((originZ - touchRay.origin.z) / touchRay.direction.z);
		}
	}
}