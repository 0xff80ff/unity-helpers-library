﻿using UnityEngine;

public class ActivePlatform : MonoBehaviour
{
  public enum Action
  {
    Activate,
    Deactivate
  }

  public Action action = Action.Activate;
  public RuntimePlatform[] platforms;

  void Awake()
  {
    bool platformFound = false;
    foreach (var platform in platforms)
      if (platform == Application.platform)
      {
        platformFound = true;
        break;
      }


    switch (action)
    {
      case Action.Activate:
        gameObject.SetActive(platformFound);
        break;
      case Action.Deactivate:
        gameObject.SetActive(!platformFound);
        break;
    }
  }
}