﻿using UnityEngine;

public class FromToCamera : MonoBehaviour
{
  [Range(0.1f, 10)]
  public float moveSpeed = 1;

  [System.Serializable]
  public class Aspect
  {
    public enum Mode
    {
      ManualPosition,
      ManualRotation,
      ObjectPosition,
      ObjectRotation
    }

    public Mode look;
    public Vector3 position;
    public Transform[] objects;
    public Vector3 objectsOffset;
  }

  public Aspect aspectFrom;
  public Aspect aspectTo;
  public Vector3 manualRotation;

  void Awake()
  {
  }

  void FixedUpdate()
  {
    Aspect f = CalcAspect(aspectFrom, null);
    Aspect t = CalcAspect(aspectTo, f);
    if (f == null)
      CalcAspect(aspectFrom, t);
    if (t == null)
      CalcAspect(aspectTo, f);

    float smoothFactor = Time.fixedDeltaTime * moveSpeed;
    Quaternion targetRotation = Quaternion.LookRotation(aspectTo.position - aspectFrom.position);
    transform.position = Vector3.Lerp(transform.position, aspectFrom.position, smoothFactor);
    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, smoothFactor);
  }

  Aspect CalcAspect(Aspect aspect, Aspect origin)
  {
    switch (aspect.look)
    {
      default:
      case Aspect.Mode.ManualPosition:
        return aspect;

      case Aspect.Mode.ManualRotation:
        if (origin == null)
          return null;
        aspect.position = origin.position - Quaternion.Euler(manualRotation) * Vector3.forward;
        return aspect;
      case Aspect.Mode.ObjectPosition:
        if (aspect.objects.Length == 0)
          return null;
        aspect.position = Vector3.zero;
        foreach (var o in aspect.objects)
          aspect.position += o.position;
        aspect.position /= aspect.objects.Length;
        aspect.position += aspect.objectsOffset;
        return aspect;
      case Aspect.Mode.ObjectRotation:
        if (origin == null || aspect.objects.Length == 0)
          return null;
        aspect.position = origin.position - Quaternion.Euler(manualRotation = aspect.objects[0].eulerAngles) * Vector3.forward;
        return aspect;
    }
  }
}
