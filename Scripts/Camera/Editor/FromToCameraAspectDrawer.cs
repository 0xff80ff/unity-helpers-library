﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(FromToCamera.Aspect))]
public class FromToCameraAspectDrawer : PropertyDrawer
{
  public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label)
  {
    rect.height = EditorGUIUtility.singleLineHeight;

    EditorGUI.BeginProperty(rect, label, prop);
    var spLook = prop.FindPropertyRelative("look");
    EditorGUI.PropertyField(rect, spLook, label);

    rect.y += rect.height;

    switch ((FromToCamera.Aspect.Mode)spLook.enumValueIndex)
    {
      case FromToCamera.Aspect.Mode.ManualPosition:
        EditorGUI.PropertyField(rect, prop.FindPropertyRelative("position"), GUIContent.none);
        break;
      case FromToCamera.Aspect.Mode.ManualRotation:
        break;
      case FromToCamera.Aspect.Mode.ObjectPosition:
      case FromToCamera.Aspect.Mode.ObjectRotation:
        var spObjects = prop.FindPropertyRelative("objects");
        rect.width -= rect.height;
        for (int i = 0; i < spObjects.arraySize; ++i)
        {
          EditorGUI.PropertyField(rect, spObjects.GetArrayElementAtIndex(i), new GUIContent("Object #" + (i + 1)));
          if (GUI.Button(new Rect(rect.x + rect.width, rect.y, rect.height, rect.height), "X"))
            spObjects.DeleteArrayElementAtIndex(i);
          rect.y += rect.height;
        }
        float lineWidth = rect.width + rect.height;
        rect.width = EditorGUIUtility.labelWidth * 0.667f;
        if (GUI.Button(rect, "Object #" + (spObjects.arraySize + 1)))
          spObjects.InsertArrayElementAtIndex(spObjects.arraySize);

        rect.width = lineWidth - EditorGUIUtility.labelWidth;
        rect.x = EditorGUIUtility.labelWidth;

        EditorGUI.PropertyField(rect, prop.FindPropertyRelative("objectsOffset"), GUIContent.none);
        break;
    }
    EditorGUI.EndProperty();
  }

  public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
  {
    float height = EditorGUIUtility.singleLineHeight;

    switch ((FromToCamera.Aspect.Mode)prop.FindPropertyRelative("look").enumValueIndex)
    {
      case FromToCamera.Aspect.Mode.ManualPosition:
        height *= 2;
        break;
      case FromToCamera.Aspect.Mode.ManualRotation:
        break;
      case FromToCamera.Aspect.Mode.ObjectPosition:
      case FromToCamera.Aspect.Mode.ObjectRotation:
        height *= (prop.FindPropertyRelative("objects").arraySize + 2);
        break;
    }

    return height + 4;
  }
}
