﻿using UnityEngine;

namespace UHL
{
	public static class Const
	{
		public const float Tiny = 1e-6f;

		public const float Golden = 1.618033989f;
		public const float GoldenInv = Golden - 1;
		public const float GoldenInvSq = 1 - GoldenInv;

		public static readonly Rect UnitRect = Rect.MinMaxRect(0, 0, 1, 1);
	}

	public static class WaitFor
	{
		public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();
		public static readonly WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();
		public static readonly WaitForSeconds Second = new WaitForSeconds(1);
	}
}