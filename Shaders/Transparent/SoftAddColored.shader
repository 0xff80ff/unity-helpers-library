﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Transparent/SoftAddColored"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="True" "ForceNoShadowCasting"="True"}
		Pass
		{
			Cull Off
			ZTest LEqual
			ZWrite Off
			Blend OneMinusDstColor One
			CGPROGRAM

			struct v2f
			{
			    float4 pos : POSITION;
			    float2 uv : TEXCOORD0;
			};

			sampler _MainTex;
			fixed4 _Color;
			 
			#pragma vertex vert
			v2f vert(
			    float4 pos : POSITION,
			    float2 uv : TEXCOORD0
			) {
				v2f o;
				o.pos = UnityObjectToClipPos(pos);
				o.uv = uv;
				return o;
			}

			#pragma fragment frag
			fixed4 frag(v2f i) : COLOR
			{
				return tex2D(_MainTex, i.uv) * _Color;
			}
				 
			ENDCG
        }  
    }
    Fallback "Mobile/Particles/Additive"
}