﻿Shader "Skybox/OneTexture"
{
	Properties
	{
		_Landscape("Landscape", 2D) = "gray" {}
	}
	SubShader
	{
		Pass
		{
			CULL BACK
			CGPROGRAM

			struct v2f
			{
			    float4 pos : POSITION;
			    half2 uv : TEXCOORD0;
			};
			 
			#pragma vertex vert
			v2f vert(
			    float4 pos : POSITION,
			    half2 uv : TEXCOORD0
			) {
				v2f o;
				o.pos = UnityObjectToClipPos(pos);
				o.uv = (uv + 1) * 0.5;
				return o;
			}

			sampler _Landscape;

			#pragma fragment frag
			fixed4 frag(v2f i) : COLOR
			{
				return tex2D(_Landscape, i.uv);
			}
				 
			ENDCG
        }  
    }

    Fallback "Skybox/Procedural"
}