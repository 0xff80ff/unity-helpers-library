﻿Shader "Procedural/RoundUnlit"
{
	Properties
	{
		_Color ("Color", Color) = (0,0,0,1)
		_T ("Sharp", Range(0, 100)) = 8
	}
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Pass
		{
			Cull Off
			ZTest LEqual
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma multi_compile __ VERTEX_COLOR

			struct v2f
			{
			    float4 pos : POSITION;
			    half2 uv : TEXCOORD1;

				#if defined(VERTEX_COLOR)
				fixed4 col : COLOR;
				#endif
			};

			half _T;

			#pragma vertex vert
			v2f vert(
			    float4 pos : POSITION,
			    half2 uv : TEXCOORD0

				#if defined(VERTEX_COLOR)
				, fixed4 col : COLOR
				#endif
			) {
				v2f o;
				o.pos = UnityObjectToClipPos(pos);
				o.uv = (uv - 0.5h) * 2.0h * sqrt(_T);

				#if defined(VERTEX_COLOR)
				o.col = col;
				#endif

				return o;
			}

			#if !defined(VERTEX_COLOR)
			fixed4 _Color;
			#endif

			#pragma fragment frag
			fixed4 frag(v2f i) : COLOR
			{
				return
				#if defined(VERTEX_COLOR)
					fixed4(i.col.rgb, i.col.a * saturate(_T - dot(i.uv, i.uv)));
				#else
					fixed4(_Color.rgb, _Color.a * saturate(_T - dot(i.uv, i.uv)));
				#endif
			}
				 
			ENDCG
        }  
    }
    Fallback "Unlit/Color"
}