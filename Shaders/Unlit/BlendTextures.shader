﻿Shader "Unlit/BlendTextures"
{
	Properties
	{
		_MainTex ("Src", 2D) = "white" {}
		_BlendTex ("Dst", 2D) = "black" {}
		_OffsetTex ("Offset", 2D) = "gray" {}
		_T ("T", Range(0, 1)) = 0.5
		_Force ("Force", Range(0.001, 0.25)) = 0.01
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"


			sampler2D _MainTex;
			sampler2D _BlendTex;
			sampler2D _OffsetTex;
			fixed _T;
			fixed _Force;

			struct v2f
			{
				float4 vertex : SV_POSITION;
				half3 uv : TEXCOORD0;
			};
			
			v2f vert (
				float4 vertex : POSITION,
				half2 uv : TEXCOORD0
			) {
				v2f o;
				o.vertex = UnityObjectToClipPos(vertex);
				o.uv = half3(uv, 1-_T);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 ds = (tex2D(_OffsetTex, i.uv.xy).xy - 0.5) * _Force;
				return lerp(
					tex2D(_MainTex, i.uv.xy + ds * _T),
					tex2D(_BlendTex, i.uv.xy - ds * i.uv.z),
					_T
				);
			}
			ENDCG
		}
	}
}
