﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Color Add"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Pass
		{
			Cull Off
			ZTest LEqual
			ZWrite Off
			Blend SrcAlpha One
			CGPROGRAM
			#pragma multi_compile __ VERTEX_COLOR

			struct v2f
			{
			    float4 pos : POSITION;

				#if defined(VERTEX_COLOR)
				fixed4 col : COLOR;
				#endif
			};

			#pragma vertex vert
			v2f vert(
				float4 pos : POSITION,
				fixed4 col : COLOR
			) {
				v2f o;
				o.pos = UnityObjectToClipPos(pos);

				#if defined(VERTEX_COLOR)
				o.col = col;
				#endif

				return o;
			}

			#if !defined(VERTEX_COLOR)
			fixed4 _Color;
			#endif

			#pragma fragment frag
			fixed4 frag(v2f i) : COLOR
			{
				#if defined(VERTEX_COLOR)
				return i.col;
				#else
				return _Color;
				#endif
			}
				 
			ENDCG
        }  
    }
    Fallback "Unlit/Color"
}