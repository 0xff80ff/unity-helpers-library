﻿using UnityEngine;

namespace UHL
{
  [System.Serializable]
  public struct ObservableProperty<T>
  {
    public delegate void OldNewValuesDelegate(T oldValue,T newValue);

    public event OldNewValuesDelegate OnChange;

    [SerializeField]
    private T _value;

    public T Value
    {
      get { return _value; }
      set { OnChange(_value, _value = value); }
    }

    public ObservableProperty(T untrackedInitialValue = default(T)) : this()
    {
      _value = untrackedInitialValue;
    }

    public static implicit operator T(ObservableProperty<T> op)
    {
      return op._value;
    }
  }
}